package database

import "belajar-golang/model"

var ModelMigrations = []interface{}{
	&model.User{},
	&model.Product{},
}