package controller

import "github.com/gofiber/fiber/v2"

func Welcome(c *fiber.Ctx) error{
	return c.JSON(fiber.Map{"status": "success", "message": "Welcome To Api Service by ndik !", "data": nil})
}