package model

import "gorm.io/gorm"

type Product struct{
	gorm.Model
	Name       string `gorm:"not null" json:"name"`
	Description string `gorm:"not null" json:"description"`
	Amount      int    `gorm:"not null" json:"amount"`
}