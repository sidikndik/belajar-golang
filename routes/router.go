package routes

import (
	"belajar-golang/controller"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)


func SetupRoutes(app *fiber.App){

	app.Use(cors.New())
	api := app.Group("/api", logger.New())
	
	api.Get("/", controller.Welcome)

	// Auth
	auth := api.Group("/auth")
	auth.Post("/login", controller.Login)

	auth.Post("/register", controller.CreateUser)
}
