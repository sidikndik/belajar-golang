# build stage
FROM golang:alpine3.19 as build
WORKDIR /app
COPY . .
RUN go mod tidy
RUN go build -o belajar-golang main.go

# run stage
FROM alpine:3.19
WORKDIR /app
COPY --from=build /app/belajar-golang .
COPY .env .

EXPOSE 8080
CMD ["/app/belajar-golang"]


