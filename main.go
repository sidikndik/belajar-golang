package main

import (
	"belajar-golang/config"
	"belajar-golang/database"
	"belajar-golang/routes"
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/spf13/viper"
)

func init(){
	config.LoadEnvironment(config.Environment)
	database.InitDatabase()

}

func main(){
	app := fiber.New()

	routes.SetupRoutes(app)
	

	fmt.Println(viper.GetString("DB_PASS"))

	log.Fatal(app.Listen(":"+ viper.GetString("PORT")))
}
